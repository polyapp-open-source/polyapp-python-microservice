import os
import json
from flask import Flask, request, abort, jsonify

app = Flask(__name__)

# TODO figure out a better way to do this, maybe by generating a library from the Go source code & updating that library when I push in changes.
class HTTPSCallRequest:
    def __init__(self, CurrentData, ReferencedData, Request, Response, BotStaticData):
        self.CurrentData = CurrentData # map[string]interface{}
        self.ReferencedData = ReferencedData # map[string]map[string]interface{}
        self.Request = Request # *common.POSTRequest
        self.Response = Response # *common.POSTResponse
        self.BotStaticData = BotStaticData # map[string]string

class HTTPSCallResponse:
    def __init__(self, Request, Response, CreateDatas, UpdateDatas, DeleteDatas):
        self.Request = Request # *common.POSTRequest
        self.Response = POSTResponse(Response["MessageID"], Response["NewURL"], Response["BrowserActions"], Response["DataUpdates"], Response["ModDOMs"]) # *common.POSTResponse
        self.CreateDatas = CreateDatas # []common.Data
        self.UpdateDatas = UpdateDatas # []common.Data
        self.DeleteDatas = DeleteDatas # []string
    def to_json(self):
        return json.dumps({"Request":self.Request,"Response":self.Response.to_dict(),"CreateDatas":self.CreateDatas,"UpdateDatas":self.UpdateDatas,"DeleteDatas":self.DeleteDatas})

class POSTResponse:
    def __init__(self, MessageID, NewURL, BrowserActions, DataUpdates, ModDOMs):
        self.MessageID = MessageID
        self.NewURL = NewURL
        self.BrowserActions = BrowserActions
        self.DataUpdates = DataUpdates
        self.ModDOMs = ModDOMs
    def to_dict(self):
        return {"MessageID":self.MessageID,"NewURL":self.NewURL,"BrowserActions":self.BrowserActions,"DataUpdates":self.DataUpdates,"ModDOMs":self.ModDOMs}
        

@app.route("/", methods=['POST'])
def polyapp_bot_service():
    if not request.is_json:
        abort(400)
    httpsCallRequest = HTTPSCallRequest(request.json["CurrentData"], request.json["ReferencedData"], request.json["Request"], request.json["Response"], request.json["BotStaticData"])
    
    httpsCallResponse = HTTPSCallResponse(httpsCallRequest.Request, httpsCallRequest.Response, None, None, None)

    probabilityOfLactoseIntolerance = (httpsCallRequest.CurrentData["Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Favorite Number"] % 100) / 100

    httpsCallResponse.Response.DataUpdates = {}
    httpsCallResponse.Response.DataUpdates["%2Ft%2FEducation%20and%20Health%20Services%2FSurvey%2FpTQLJsSuRmeGSbmbmmWkUKYhY%3Fux=ULqtHVGPCIBkuEfsuiEABIyfg&schema=ODPZbcXuqQOLMlFYNzVnPvqur&data=WSfjSXkUpzDCqNvnGyseMbQNt"] = {
        "polyappIndustryID": "Education and Health Services",
        "polyappDomainID": "Survey",
        "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Probability You Are Lactose Intolerant": probabilityOfLactoseIntolerance,
        "polyappSchemaID": "ODPZbcXuqQOLMlFYNzVnPvqur",
    }
    return httpsCallResponse.to_json()


# ---------------- Request Example ----------------
#
# {
# "ReferencedData": {},
# "CurrentData": {
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Favorite Color": "Red",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Load User IDs": [
#     "dBxKPjxhvUbznNCPaIMsgoSpI"
#     ],
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Email": "g.ledray@gmail.com",
#     "polyappSchemaID": "ODPZbcXuqQOLMlFYNzVnPvqur",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Load Timestamps": [
#     1629234859
#     ],
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Do you like milk?": "Yes",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Favorite Number": 40,
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Done Timestamps": [
#     1628179297,
#     1628183231
#     ],
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Do you drink coffee daily?": "Yes",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Done User IDs": [
#     "dBxKPjxhvUbznNCPaIMsgoSpI",
#     "dBxKPjxhvUbznNCPaIMsgoSpI"
#     ],
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Done": false,
#     "polyappDomainID": "Survey",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Do you use cream in your coffee?": "Yes",
#     "polyappIndustryID": "Education and Health Services",
#     "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Probability You Are Lactose Intolerant": 0.5
# },
# "BotStaticData": {
#     "HTTPSCall": "POST https://test-prediction-survey-bot-lqfzuu2qhq-uc.a.run.app"
# },
# "Request": {
#     "RoleID": "dBxKPjxhvUbznNCPaIMsgoSpI",
#     "MessageID": "d9370eb4-16e1-4f43-a709-09d3a641afef",
#     "IndustryID": "Education and Health Services",
#     "TaskID": "pTQLJsSuRmeGSbmbmmWkUKYhY",
#     "UXCache": null,
#     "ModifyID": "polyappNone",
#     "FinishURL": "",
#     "SchemaID": "ODPZbcXuqQOLMlFYNzVnPvqur",
#     "DataID": "WSfjSXkUpzDCqNvnGyseMbQNt",
#     "IsDone": false,
#     "SchemaCache": null,
#     "OverrideIndustryID": "polyappNone",
#     "Data": {
#     "%2Ft%2FEducation%20and%20Health%20Services%2FSurvey%2FpTQLJsSuRmeGSbmbmmWkUKYhY%3Fux=ULqtHVGPCIBkuEfsuiEABIyfg&schema=ODPZbcXuqQOLMlFYNzVnPvqur&data=WSfjSXkUpzDCqNvnGyseMbQNt": {
#         "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Probability You Are Lactose Intolerant": 0.5,
#         "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Favorite Number": 40,
#         "polyappIndustryID": "Education and Health Services",
#         "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Email": "g.ledray@gmail.com",
#         "polyappSchemaID": "ODPZbcXuqQOLMlFYNzVnPvqur",
#         "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Do you like milk?": "Yes",
#         "polyappDomainID": "Survey"
#     }
#     },
#     "DomainID": "Survey",
#     "OverrideTaskID": "polyappNone",
#     "UserID": "dBxKPjxhvUbznNCPaIMsgoSpI",
#     "WantDocuments": {
#     "UX": [],
#     "Role": [],
#     "Schema": [],
#     "User": [],
#     "Task": [],
#     "Data": []
#     },
#     "RecaptchaResponse": "",
#     "PublicMapOwningUser": {
#     "BotsTriggeredAtDone": null,
#     "ThemePath": null,
#     "Email": null,
#     "Roles": null,
#     "PhotoURL": null,
#     "PhoneNumber": null,
#     "HomeUX": "",
#     "EmailVerified": null,
#     "polyappDeprecated": null,
#     "polyappFirestoreID": "",
#     "HomeData": "",
#     "BotsTriggeredAtLoad": null,
#     "HomeSchema": "",
#     "FullName": null,
#     "UID": null,
#     "HomeTask": ""
#     },
#     "UserCache": {
#     "EmailVerified": true,
#     "FullName": "Gregory Ledray",
#     "polyappDeprecated": null,
#     "HomeTask": "DisplayDashboard",
#     "BotsTriggeredAtDone": [],
#     "ThemePath": "",
#     "PhoneNumber": "",
#     "polyappFirestoreID": "dBxKPjxhvUbznNCPaIMsgoSpI",
#     "Email": "greg@polyapp.tech",
#     "HomeUX": "DisplayDashboard",
#     "UID": "6RmrsXbQc5Th9yyibkmHJKVZvnh2",
#     "HomeData": "KkthcubYmJmSgOUWAWhbgRive",
#     "HomeSchema": "DisplayDashboard",
#     "Roles": [
#         "polyappAdminRole",
#         "WvJgPpCHpVoWTxwJywdztAXTq"
#     ],
#     "BotsTriggeredAtLoad": [
#         "OrQZUtWFxbuFlOywRCmVghFOk"
#     ],
#     "PhotoURL": "https://lh3.googleusercontent.com/a-/AOh14GhezDqRRjGSISfhvrLaWTbDZW_Pog9FWsZvMRtJ=s96-c"
#     },
#     "TaskCache": null,
#     "PublicPath": "",
#     "UXID": "ULqtHVGPCIBkuEfsuiEABIyfg",
#     "OverrideDomainID": "polyappNone"
# },
# "Response": {
#     "UXDocuments": [],
#     "MessageID": "d9370eb4-16e1-4f43-a709-09d3a641afef",
#     "NewURL": "",
#     "DataUpdates": {},
#     "BrowserActions": [],
#     "TaskDocuments": [],
#     "SchemaDocuments": [],
#     "ModDOMs": [
#     {
#         "DeleteSelector": "[id^=polyappError] > *",
#         "AddClassSelector": "",
#         "RemoveClassSelector": ".is-invalid",
#         "RemoveClasses": [
#         "is-invalid"
#         ],
#         "Action": "",
#         "AddClasses": null,
#         "HTML": "",
#         "InsertSelector": ""
#     },
#     {
#         "DeleteSelector": "#polyappErrorDone",
#         "InsertSelector": "#Done",
#         "RemoveClasses": null,
#         "AddClassSelector": "",
#         "RemoveClassSelector": "",
#         "Action": "beforeend",
#         "AddClasses": null,
#         "HTML": "<div id=\"polyappErrorDone></div>"
#     },
#     {
#         "AddClasses": null,
#         "DeleteSelector": "#polyappJSPOSTError",
#         "RemoveClasses": null,
#         "Action": "",
#         "HTML": "",
#         "InsertSelector": "",
#         "AddClassSelector": "",
#         "RemoveClassSelector": ""
#     }
#     ],
#     "RoleDocuments": [],
#     "DataDocuments": [],
#     "UserDocuments": []
# }
# }
# ---------------- End Request Example ----------------
#
# ---------------- Response Example ----------------
# {
# "UpdateDatas": [],
# "DeleteDatas": [],
# "CreateDatas": [],
# "Request": {
#     "TaskID": "",
#     "PublicPath": "",
#     "OverrideIndustryID": "",
#     "WantDocuments": {
#     "Task": null,
#     "Data": null,
#     "Role": null,
#     "Schema": null,
#     "UX": null,
#     "User": null
#     },
#     "UXID": "",
#     "UXCache": null,
#     "UserID": "",
#     "DomainID": "",
#     "TaskCache": null,
#     "RoleID": "",
#     "ModifyID": "",
#     "SchemaCache": null,
#     "IndustryID": "",
#     "Data": null,
#     "SchemaID": "",
#     "UserCache": null,
#     "OverrideTaskID": "",
#     "IsDone": false,
#     "OverrideDomainID": "",
#     "PublicMapOwningUser": {
#     "polyappDeprecated": null,
#     "ThemePath": null,
#     "HomeSchema": "",
#     "HomeTask": "",
#     "polyappFirestoreID": "",
#     "PhoneNumber": null,
#     "PhotoURL": null,
#     "EmailVerified": null,
#     "HomeUX": "",
#     "Email": null,
#     "BotsTriggeredAtDone": null,
#     "BotsTriggeredAtLoad": null,
#     "FullName": null,
#     "Roles": null,
#     "HomeData": "",
#     "UID": null
#     },
#     "FinishURL": "",
#     "MessageID": "",
#     "RecaptchaResponse": "",
#     "DataID": ""
# },
# "Response": {
#     "NewURL": "",
#     "BrowserActions": null,
#     "UserDocuments": null,
#     "TaskDocuments": null,
#     "UXDocuments": null,
#     "DataDocuments": null,
#     "RoleDocuments": null,
#     "DataUpdates": {
#     "%2Ft%2FEducation%20and%20Health%20Services%2FSurvey%2FpTQLJsSuRmeGSbmbmmWkUKYhY%3Fux=ULqtHVGPCIBkuEfsuiEABIyfg&schema=ODPZbcXuqQOLMlFYNzVnPvqur&data=WSfjSXkUpzDCqNvnGyseMbQNt": {
#         "polyappIndustryID": "Education and Health Services",
#         "polyappDomainID": "Survey",
#         "Education and Health Services_Survey_ODPZbcXuqQOLMlFYNzVnPvqur_Probability You Are Lactose Intolerant": 0.1,
#         "polyappSchemaID": "ODPZbcXuqQOLMlFYNzVnPvqur"
#     }
#     },
#     "MessageID": "",
#     "SchemaDocuments": null,
#     "ModDOMs": null
# }
# }
# 
# ---------------- End Response Example ----------------

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))