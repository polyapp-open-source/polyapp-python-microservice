PROJECT_NAME_SIMPLE=polyapp-python-microservice
# CONNECTOR_NAME=cloudrunconnector

GOOGLE_CLOUD_PROJECT_ID=$(gcloud config get-value project)
# REGION=$(gcloud app describe --format="value(locationId.scope())")
# SQL_REGION=$(gcloud compute zones list --filter="region ~ $REGION" --limit=1 --format="value(region)")
# VPC_NETWORK=default

gcloud services enable vpcaccess.googleapis.com

gcloud builds submit --tag gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --project $GOOGLE_CLOUD_PROJECT_ID

# NETWORKS=$(gcloud compute networks vpc-access connectors list --filter="name=projects/$GOOGLE_CLOUD_PROJECT_ID/locations/$SQL_REGION/connectors/$CONNECTOR_NAME" --region=$SQL_REGION)
# if [ "$NETWORKS" = "" ]
# then
#     # There is no network set up already
#     gcloud compute networks vpc-access connectors create $CONNECTOR_NAME --region $SQL_REGION --min-instances 2 --max-instances 10 --network $VPC_NETWORK --range 10.64.0.0/28
# else
#     # There is already a network set up
# fi

gcloud run deploy $PROJECT_NAME_SIMPLE --image gcr.io/$GOOGLE_CLOUD_PROJECT_ID/$PROJECT_NAME_SIMPLE --platform managed --quiet --region us-central1 --project $GOOGLE_CLOUD_PROJECT_ID  --set-env-vars=GOOGLE_CLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT_ID --allow-unauthenticated
# --vpc-connector=$CONNECTOR_NAME --vpc-egress=all --ingress=internal