# Polyapp Python Microservice
[Polyapp's](https://gitlab.com/polyapp-open-source/polyapp) HTTPS Bot calls an arbitrary URL, which means you can write code which closely integrates with Polyapp in any language you desire.

This respository demonstrates a service which affects a UI element in Polyapp. It is a *proof of concept*. If you have a need for this functionality in a production service, please email support@polyapp.tech and we can walk through your options.

# Build and Run
pip install flask

python main.py

# Deploy
## Option 1: Manual Deployment
https://console.cloud.google.com/

git clone [your Git Repo]

cd [your Git Repo]

./google_cloud_deploy.sh

# Integrate
You'll want to call your microservice using the HTTPSCall Bot. That Bot is documented <a href="https://polyapp-public.appspot.com/t/polyapp/Bot/eoUIvRnzeFAwpWXRYUDAgCcsT?ux=wEuFHLqvricbzBxgfdWqwljjH&schema=veKWxEWtCEnCDLknytuUPzMHY&data=wCHhssCsBXvqyHPkMwPwKldnC">here on the public instance</a> and at this relative URL: <code>/t/polyapp/Bot/eoUIvRnzeFAwpWXRYUDAgCcsT?ux=wEuFHLqvricbzBxgfdWqwljjH&schema=veKWxEWtCEnCDLknytuUPzMHY&data=wCHhssCsBXvqyHPkMwPwKldnC</code>

Essentially, you will use the HTTPSCallBot's Bot ID "dplFLIDbLxEpMicZPIZBfRElq" and configure it by adding a BotStaticData which looks something like this: <code>HTTPSCall POST https://test-prediction-survey-bot-SOMETHING-uc.a.run.app</code> except your URL will be the Cloud Run URL in your instance. Make sure you use POST.

# Manual Testing
You can now test the entire system by ensuring that a Data which is used by the Task calls your Microservice Bot when expected and it performs the expected work.
